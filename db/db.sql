CREATE TABLE employee (
  Id INTEGER PRIMARY KEY,
  Name VARCHAR(100)
);

INSERT INTO employee (Id, Name) VALUES (1, 'Rushi');