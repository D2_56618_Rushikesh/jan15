const express=require('express')
const db=require('./db')
const utils=require('./utils')
const cors=require('cors')

const app=express()
app.use(cors('*'))
app.use(express.json())

app.get('/', (request, response)=>{
    response.send('shree ganesha')
})

app.get('/employee', (request, response) => {
    const query = `
      SELECT Id, Name
      FROM employee `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
  app.post('/', (request, response) => {
    const { id, name } = request.body
  
    const query = `
      INSERT INTO employee
        (Id, Name)
      VALUES
        ('${id}','${name}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

app.listen(4000, '0.0.0.0', ()=>{
    console.log('server started on port 4000')
})